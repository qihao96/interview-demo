run as  ``` python demo.py``` 


output
```
the Original size of image is 512x512
the upper left corner of image is (0,0)
-------------
into 1 th window
thw Window chosen is from (83,135) to (320,473)
the calc Mean is 128.996021
the calc Variance is 2823.314912
the numpy Mean is 128.996021
the numpy Variance is 2823.314912
-------------
into 2 th window
thw Window chosen is from (157,265) to (501,458)
the calc Mean is 135.367100
the calc Variance is 2431.635834
the numpy Mean is 135.367100
the numpy Variance is 2431.635834
-------------
into 3 th window
thw Window chosen is from (275,86) to (297,469)
the calc Mean is 117.029099
the calc Variance is 2230.113623
the numpy Mean is 117.029099
the numpy Variance is 2230.113623
-------------
into 4 th window
thw Window chosen is from (1,313) to (301,391)
the calc Mean is 137.377055
the calc Variance is 2681.203134
the numpy Mean is 137.377055
the numpy Variance is 2681.203134
-------------
into 5 th window
thw Window chosen is from (41,31) to (148,328)
the calc Mean is 141.221508
the calc Variance is 1418.208733
the numpy Mean is 141.221508
the numpy Variance is 1418.208733
-------------
into 6 th window
thw Window chosen is from (26,248) to (353,306)
the calc Mean is 155.343944
the calc Variance is 1557.332922
the numpy Mean is 155.343944
the numpy Variance is 1557.332922
-------------
into 7 th window
thw Window chosen is from (405,58) to (483,160)
the calc Mean is 74.941870
the calc Variance is 1300.408443
the numpy Mean is 74.941870
the numpy Variance is 1300.408443
-------------
into 8 th window
thw Window chosen is from (292,56) to (452,212)
the calc Mean is 85.964751
the calc Variance is 1688.675143
the numpy Mean is 85.964751
the numpy Variance is 1688.675143
-------------
into 9 th window
thw Window chosen is from (383,101) to (476,318)
the calc Mean is 101.156793
the calc Variance is 1923.249523
the numpy Mean is 101.156793
the numpy Variance is 1923.249523
-------------
into 10 th window
thw Window chosen is from (150,250) to (216,383)
the calc Mean is 159.650702
the calc Variance is 2148.850590
the numpy Mean is 159.650702
the numpy Variance is 2148.850590
-------------
the Calculation is finished
```