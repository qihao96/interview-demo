import sys
import random
import numpy as np
import scipy.io as sio


def pre_process(img):
    img1 = np.zeros(img.shape)
    img2 = np.zeros(img.shape)
    # for mean pre process
    for i in range(1, img1.shape[0]):
        for j in range(1, img1.shape[1]):
            img1[i, j] = img[i, j]+img1[i-1, j]+img1[i, j-1]-img1[i-1, j-1]

    # for var pre process
    for i in range(1, img2.shape[0]):
        for j in range(1, img2.shape[1]):
            img2[i, j] = (img[i, j]**2)+img2[i-1, j] + \
                img2[i, j-1]-img2[i-1, j-1]

    return img1, img2


def prefix_sum(img, img1, img2, x, y, xx, yy):
    # (img[xx,yy]-img[x-1,yy]-img[xx,y-1]+img[x-1,y-1])
    # prefix sum for mean
    rec_sum_1 = (img1[xx, yy]-img1[x-1, yy] -
                 img1[xx, y-1]+img1[x-1, y-1])

    # prefix sum for var
    rec_sum_2 = (img2[xx, yy]-img2[x-1, yy] -
                 img2[xx, y-1]+img2[x-1, y-1])
    return rec_sum_1, rec_sum_2


# use numpy for validation
def get_mean_var(win):
    # print(win.shape)
    pixel_mean = np.mean(win)
    pixel_var = np.square(np.std(win))
    return pixel_mean, pixel_var


# load image from lena test mat file
def loadImg():
    img = sio.loadmat('lena512.mat')['lena512']
    # print(img.shape)
    M = img.shape[0]  # row number
    N = img.shape[1]  # column number
    print("the Original size of image is %dx%d" % (M, N))
    print("the upper left corner of image is (0,0)")
    print("-------------")
    return img, M, N


# generate candiate Windows from MxN original image
# (x,y) --- (x,yy)
#   |           |
#   |           |
# (xx,y)----(xx,yy)
def generateWindow(img, M, N):
    # (x,y) is the upper left corner of Window
    x = random.randint(1, M)
    y = random.randint(1, N)
    # row number of Window
    a = random.randint(1, M)
    # column number of Window
    b = random.randint(1, N)
    xx = x+a
    yy = y+b
    win = img[:, y:yy+1]
    win = win[x:xx+1, :]
    return win, x, y, xx, yy


def main():
    img, M, N = loadImg()
    img1, img2 = pre_process(img)

    # K = int(sys.argv[1])
    # K = int(1e6)
    K = 10
    for t in range(1, K+1):
        print("into %d th window" % t)
        win, x, y, xx, yy = generateWindow(img, M, N)
        while xx > M or yy > N:
            win, x, y, xx, yy = generateWindow(img, M, N)

        print("thw Window chosen is from (%d,%d) to (%d,%d)" % (x, y, xx, yy))

        rec_sum_1, rec_sum_2 = prefix_sum(img, img1, img2, x, y, xx, yy)

        # mean = E[X]
        mean = rec_sum_1/win.size
        # Var = E[X^2]-E[X]^2
        var = rec_sum_2/win.size - (mean**2)
        print("the calc Mean is %f" % mean)
        print("the calc Variance is %f" % var)

        # numpy validation
        np_mean, np_var = get_mean_var(win)
        print("the numpy Mean is %f" % np_mean)
        print("the numpy Variance is %f" % np_var)

        print("-------------")


if __name__ == "__main__":
    main()
    print("the Calculation is finished")