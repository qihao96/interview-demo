import random
# infinite length single linked list
# W1->W2->W3->.......>infinite node
class LinkNode(object):
    def __init__(self,x):
        self.val = x
        self.next = None

def generateWebs():
    w1 = "https://xyzrobotics.ai"
    head = LinkNode(w1)
    # omit specific operations to generate W1->W2->W3->...
    return head

def equalReturn():
    node = generateWebs()
    n = 0
    result = 0
    while node:
        if random.randint(0,n) == 0:
            result = node.val
        node = node.next
        n += 1

    return result